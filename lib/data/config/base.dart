import 'package:http/http.dart' as http;

class API {
  // URL Base de PokeApi.cos
  final String _urlAPI = 'https://pokeapi.co';
  // URL Base de imagenes PokeApi.cos
  final String _urlApiImage =
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon';

  String get urlApi => this._urlAPI;
  String get urlApiImage => this._urlApiImage;

  getData(apiUrl, headers) async {
    final fullURl = _urlAPI + apiUrl;
    return await http.get(
      fullURl,
      headers: headers,
    );
  }
}
