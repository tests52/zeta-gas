import 'dart:io';
import 'dart:convert';

import 'package:pokedex/data/config/base.dart';
import 'package:pokedex/domain/model/pokemon.dart';
import 'package:pokedex/domain/model/stats.dart';
import 'package:pokedex/domain/response/pokemon/list.dart';
import 'package:pokedex/domain/repository/api/pokemon_repository.dart';
import 'package:pokedex/domain/response/pokemon/stats.dart';

class ApiPokemonRepositoryImpl implements ApiPokemonRepositoryInterface {
  final API api = new API();

  @override
  Future<PokemonResponse> list() async {
    try {
      final response = await api.getData('/api/v2/pokemon', {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.acceptHeader: 'application/json',
      });

      switch (response.statusCode) {
        case 200:
          final decodedResp = json.decode(response.body);
          List<Pokemon> list = [];

          for (var item in decodedResp['results']) {
            final String url = item['url'];
            final split = url.split('/');

            Pokemon model = new Pokemon(
              id: int.parse(split[split.length - 2]),
              name: item['name'],
              url: url,
            );
            list.add(model);
          }
          return PokemonResponse(
            results: list,
          );
          break;
        default:
          throw UnimplementedError();
          break;
      }
    } finally {
      //
    }
  }

  @override
  Future<PokemonStatsResponse> stats(int id) async {
    try {
      final response = await api.getData('/api/v2/pokemon/${id.toString()}/', {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.acceptHeader: 'application/json',
      });

      switch (response.statusCode) {
        case 200:
          final decodedResp = json.decode(response.body);
          List<PokemonStat> list = [];

          for (var item in decodedResp['stats']) {
            //
            PokemonStat model = new PokemonStat(
              baseStat: item['base_stat'],
              name: item['stat']['name'],
            );
            list.add(model);
          }
          return PokemonStatsResponse(
            stats: list,
          );
          break;
        default:
          throw UnimplementedError();
          break;
      }
    } finally {
      //
    }
  }

  @override
  Future<List<String>> abilities(int id) async {
    try {
      final response = await api.getData('/api/v2/pokemon/${id.toString()}/', {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.acceptHeader: 'application/json',
      });

      switch (response.statusCode) {
        case 200:
          final decodedResp = json.decode(response.body);
          List<String> list = [];

          for (var item in decodedResp['abilities']) {
            list.add(
              item['ability']['name'],
            );
          }
          return list;
          break;
        default:
          throw UnimplementedError();
          break;
      }
    } finally {
      //
    }
  }
}
