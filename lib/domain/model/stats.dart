class PokemonStat {
  //
  PokemonStat({
    this.baseStat,
    this.name,
  });

  final int baseStat;
  final String name;
}
