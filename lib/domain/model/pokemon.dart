class Pokemon {
  //
  Pokemon({
    this.id,
    this.name,
    this.url,
  });
  //
  final int id;
  final String name;
  final String url;
}
