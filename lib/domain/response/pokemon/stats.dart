//
import 'package:pokedex/domain/model/stats.dart';

class PokemonStatsResponse {
  //
  PokemonStatsResponse({
    this.stats,
  });
  //
  final List<PokemonStat> stats;
}
