//
import 'package:pokedex/domain/model/pokemon.dart';

class PokemonResponse {
  //
  PokemonResponse({
    this.results,
  });
  //
  final List<Pokemon> results;
}
