import 'package:pokedex/domain/response/pokemon/list.dart';
import 'package:pokedex/domain/response/pokemon/stats.dart';

abstract class ApiPokemonRepositoryInterface {
  // Metodo para solicitar la informacion de listado de resultados de pokemons
  Future<PokemonResponse> list();
  Future<PokemonStatsResponse> stats(int id);
  Future<List<String>> abilities(int id);
}
