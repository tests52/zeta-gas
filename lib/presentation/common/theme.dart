import 'package:flutter/material.dart';

class PokedexColors {
  static final red = Color(0xFFec6c6d);
  static final littleRed = Color(0xFFee7b70);
  static final orange = Color(0xFFf09375);
}

class LinearGradientsColors {
  static final backgroundColor = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      PokedexColors.red,
      PokedexColors.orange,
    ],
  );
}

class BoxShadows {
  static final boxShadow = [
    BoxShadow(
      color: Colors.black.withOpacity(0.15),
      spreadRadius: 4,
      blurRadius: 8,
      offset: Offset(1, 3),
    ),
  ];
}
