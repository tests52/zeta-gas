import 'package:flutter/material.dart';

class PokemonAbilitieWidget extends StatelessWidget {
  const PokemonAbilitieWidget({
    Key key,
    @required this.abilitie,
  }) : super(key: key);

  final String abilitie;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 175,
      height: 50,
      margin: EdgeInsets.only(right: 25),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Center(
        child: Text(
          abilitie,
          style: TextStyle(color: Colors.grey[600]),
        ),
      ),
    );
  }
}
