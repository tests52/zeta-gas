import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pokedex/presentation/common/theme.dart';

class PokeListWidget extends StatelessWidget {
  PokeListWidget({
    @required this.id,
    @required this.name,
    @required this.iconImage,
    this.callback,
  });

  final int id;
  final String name;
  final String iconImage;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return BounceInRight(
      duration: Duration(milliseconds: 500),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: Material(
            color: Colors.white,
            child: InkWell(
              splashColor: Colors.grey[100],
              onTap: this.callback,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 25),
                width: MediaQuery.of(context).size.width,
                height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Pulse(
                          duration: Duration(milliseconds: 900),
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              boxShadow: BoxShadows.boxShadow,
                              gradient: LinearGradientsColors.backgroundColor,
                            ),
                            child: Image.network(
                              this.iconImage,
                              width: 50,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          this.name,
                          style: TextStyle(
                            color: PokedexColors.littleRed,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                    SvgPicture.asset(
                      'assets/icons/arrow_rigth.svg',
                      color: PokedexColors.littleRed,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
