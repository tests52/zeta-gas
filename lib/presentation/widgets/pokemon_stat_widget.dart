import 'package:flutter/material.dart';
import 'package:pokedex/presentation/common/theme.dart';

class AttributeWidget extends StatelessWidget {
  const AttributeWidget({
    this.stats,
    this.nameAttr,
  });

  final String nameAttr;
  final int stats;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        children: [
          RotatedBox(
            quarterTurns: -1,
            child: Container(
              width: MediaQuery.of(context).size.width / 2.5,
              child: Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: TweenAnimationBuilder(
                    tween: Tween<double>(begin: 0.0, end: this.stats / 100),
                    duration: const Duration(milliseconds: 1000),
                    curve: Curves.easeInOut,
                    builder: (context, value, child) {
                      return LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: AlwaysStoppedAnimation<Color>(
                          PokedexColors.orange,
                        ),
                        value: value,
                      );
                    },
                  ),
                ),
              ),
            ),
          ),
          Text(
            '${this.stats.toString()} %',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
