import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';
import 'package:pokedex/presentation/common/theme.dart';

class PokemonItemDescStat extends StatelessWidget {
  const PokemonItemDescStat({
    @required this.nameAttr,
    @required this.stats,
    @required this.index,
  });

  final String nameAttr;
  final int stats;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 25),
        margin: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              this.nameAttr.toUpperCase(),
              style: TextStyle(
                color: Colors.grey[600],
              ),
            ),
            BounceInLeft(
              duration: Duration(milliseconds: 1000),
              delay: Duration(milliseconds: 1000 * this.index),
              child: CircleAvatar(
                backgroundColor: PokedexColors.littleRed,
                child: Text(
                  this.stats.toString(),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
