import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pokedex/presentation/common/theme.dart';
import 'package:pokedex/presentation/pages/home/home_screen.dart';
import 'package:pokedex/presentation/pages/splash/splash_bloc.dart';

class SplashScreen extends StatefulWidget {
  // Creo una instancia privada de mi clase para inicializar mi controlador BLoC
  SplashScreen._();

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => SplashBLoC(),
      builder: (_, __) => SplashScreen._(),
    );
  }

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<void> _init() async {
    final bloc = context.read<SplashBLoC>();

    final result = await bloc.validateSession();

    if (result) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext newContext) => HomeScreen.init(newContext),
        ),
      );
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _init();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _page(context),
    );
  }

  Widget _page(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradientsColors.backgroundColor,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/logo.png',
            width: 350,
          ),
          SizedBox(
            height: 100,
          ),
          Image.asset(
            'assets/images/pokebola.png',
            width: 250,
          ),
        ],
      ),
    );
    //
  }
}
