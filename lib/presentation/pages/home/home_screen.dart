import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/presentation/pages/home/detail/pokemon_detail.dart';
import 'package:provider/provider.dart';
import 'package:pokedex/domain/model/pokemon.dart';
import 'package:pokedex/presentation/common/theme.dart';
import 'package:pokedex/presentation/pages/home/home_bloc.dart';
import 'package:pokedex/domain/repository/api/pokemon_repository.dart';
import 'package:pokedex/presentation/widgets/pokemon_item_widget.dart';

class HomeScreen extends StatefulWidget {
  // Creo una instancia privada de mi clase para inicializar mi controlador BLoC
  HomeScreen._();

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => HomeBLoC(
        apiPokemonRepositoryInterface:
            context.read<ApiPokemonRepositoryInterface>(),
      ),
      builder: (_, __) => HomeScreen._(),
    );
  }

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() {
    final bloc = Provider.of<HomeBLoC>(context, listen: false);
    bloc.pokemonsList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            gradient: LinearGradientsColors.backgroundColor,
          ),
          child: Column(
            children: [
              _header(context),
              _pokemonList(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _header(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      margin: EdgeInsets.only(bottom: 10),
      width: MediaQuery.of(context).size.width,
      height: 100,
      child: Center(
        child: BounceInDown(
          duration: Duration(milliseconds: 400),
          child: Image.asset(
            'assets/images/logo.png',
            width: 175,
          ),
        ),
      ),
    );
  }

  Widget _pokemonList(BuildContext context) {
    return Consumer<HomeBLoC>(
      builder: (context, bloc, child) {
        return Expanded(
          child: ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: bloc.pokemonList.length,
            itemBuilder: (context, index) {
              Pokemon pokemon = bloc.pokemonList[index];
              return PokeListWidget(
                id: pokemon.id,
                name: pokemon.name,
                iconImage: bloc.getImageIconUrl(pokemon.id),
                callback: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext newContext) =>
                          PokemonDetailScreen.init(newContext),
                      settings: RouteSettings(
                        arguments: pokemon,
                      ),
                    ),
                  );
                },
              );
            },
          ),
        );
      },
    );
  }
}
