import 'package:flutter/material.dart';
import 'package:pokedex/domain/model/pokemon.dart';
import 'package:pokedex/domain/repository/api/pokemon_repository.dart';

class HomeBLoC extends ChangeNotifier {
  final ApiPokemonRepositoryInterface apiPokemonRepositoryInterface;
  //
  HomeBLoC({
    this.apiPokemonRepositoryInterface,
  });

  List<Pokemon> pokemonList = [];

  // Aqui vamos a listar los metodos
  Future<void> pokemonsList() async {
    final response = await apiPokemonRepositoryInterface.list();
    pokemonList = response.results;
    notifyListeners();
  }

  String getImageIconUrl(int id) {
    return 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id.toString()}.png';
  }
}
