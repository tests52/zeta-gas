import 'package:flutter/material.dart';
import 'package:pokedex/domain/model/pokemon.dart';
import 'package:pokedex/domain/model/stats.dart';
import 'package:pokedex/domain/repository/api/pokemon_repository.dart';

class PokemonDetailBLoC extends ChangeNotifier {
  //
  final ApiPokemonRepositoryInterface apiPokemonRepositoryInterface;
  //
  PokemonDetailBLoC({
    this.apiPokemonRepositoryInterface,
  });

  Pokemon pokemon = new Pokemon();
  List<PokemonStat> pokemonListStats = [];
  List<String> abilities = [];

  String getImageUrl(int id) {
    return 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${id.toString()}.svg';
  }

  // Este metodo me permite obtener las estadisticias luego de obtener el parametro por la URL
  Future<void> pokemonStat(int id) async {
    // await Future.delayed(Duration(seconds: 2));
    final response = await apiPokemonRepositoryInterface.stats(id);
    pokemonListStats = response.stats;
    notifyListeners();
  }

  // Este metodo me permite obtener las abilidades luego de obtener el parametro por la URL
  Future<void> pokemonAbilities(int id) async {
    // await Future.delayed(Duration(seconds: 2));
    final response = await apiPokemonRepositoryInterface.abilities(id);
    abilities = response;
    notifyListeners();
  }
}
