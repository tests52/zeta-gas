import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pokedex/domain/model/stats.dart';
import 'package:pokedex/domain/model/pokemon.dart';
import 'package:pokedex/presentation/common/theme.dart';
import 'package:pokedex/presentation/widgets/pokemon_stat_widget.dart';
import 'package:pokedex/domain/repository/api/pokemon_repository.dart';
import 'package:pokedex/presentation/widgets/pokemon_abilitie_widget.dart';
import 'package:pokedex/presentation/widgets/pokemon_stat_desc_widget.dart';
import 'package:pokedex/presentation/pages/home/detail/pokemon_detail_bloc.dart';

class PokemonDetailScreen extends StatefulWidget {
  // Creo una instancia privada de mi clase para inicializar mi controlador BLoC
  PokemonDetailScreen._();

  static Widget init(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => PokemonDetailBLoC(
        apiPokemonRepositoryInterface:
            context.read<ApiPokemonRepositoryInterface>(),
      ),
      builder: (_, __) => PokemonDetailScreen._(),
    );
  }

  @override
  _PokemonDetailScreenState createState() => _PokemonDetailScreenState();
}

class _PokemonDetailScreenState extends State<PokemonDetailScreen> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final bloc = Provider.of<PokemonDetailBLoC>(context, listen: false);
      bloc.pokemonAbilities(bloc.pokemon.id);
      bloc.pokemonStat(bloc.pokemon.id);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //
    final pokemon = ModalRoute.of(context).settings.arguments as Pokemon;
    final bloc = Provider.of<PokemonDetailBLoC>(context, listen: false);
    bloc.pokemon = pokemon;

    //
    return Scaffold(
      body: SafeArea(
        child: _page(context, pokemon),
      ),
    );
  }

  Widget _page(BuildContext context, Pokemon pokemon) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Column(
          children: [
            //
            _header(context, pokemon),
            //
            _abilities(context),
            //
            _graficStats(context),
            //
            _statsList(context),
          ],
        ),
      ),
    );
  }

  Widget _header(BuildContext context, Pokemon pokemon) {
    return Consumer<PokemonDetailBLoC>(
      builder: (context, bloc, child) {
        return Stack(
          children: [
            Positioned(
              top: 25,
              left: 25,
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: PokedexColors.littleRed,
                  size: 30,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 3,
              child: Center(
                child: Container(
                  width: 200,
                  height: 200,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    boxShadow: BoxShadows.boxShadow,
                    gradient: LinearGradientsColors.backgroundColor,
                  ),
                  child: Center(
                    child: SvgPicture.network(
                      bloc.getImageUrl(pokemon.id),
                      width: 125,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 50,
              right: 100,
              child: Image.asset(
                'assets/images/pokebola.png',
                width: 75,
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _abilities(BuildContext context) {
    return Consumer<PokemonDetailBLoC>(
      builder: (context, bloc, child) {
        if (bloc.pokemonListStats.length == 0) {
          return Container(
            width: MediaQuery.of(context).size.width - 20,
            height: MediaQuery.of(context).size.height / 2,
            padding: EdgeInsets.only(top: 50),
            child: Center(
              child: Text('Cargando...'),
            ),
          );
        } else {
          return Container(
            width: MediaQuery.of(context).size.width - 75,
            height: 200,
            margin: EdgeInsets.only(bottom: 25),
            decoration: BoxDecoration(
              boxShadow: BoxShadows.boxShadow,
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradientsColors.backgroundColor,
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 25,
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    'Abilities',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  flex: 2,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.only(left: 25),
                    itemCount: bloc.abilities.length,
                    itemBuilder: (context, index) {
                      String abilitie = bloc.abilities[index];
                      return PokemonAbilitieWidget(abilitie: abilitie);
                    },
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
              ],
            ),
          );
        }
      },
    );
  }

  Widget _graficStats(BuildContext context) {
    return Consumer<PokemonDetailBLoC>(
      builder: (context, bloc, child) {
        if (bloc.pokemonListStats.length == 0) {
          return Container(
            width: MediaQuery.of(context).size.width - 20,
            height: MediaQuery.of(context).size.height / 2,
            padding: EdgeInsets.only(top: 50),
            child: Center(
              child: Text('Cargando...'),
            ),
          );
        } else {
          return Container(
            width: MediaQuery.of(context).size.width - 75,
            height: 300,
            margin: EdgeInsets.only(bottom: 25),
            decoration: BoxDecoration(
              boxShadow: BoxShadows.boxShadow,
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradientsColors.backgroundColor,
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 25,
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    'Grafic Stats',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  flex: 6,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.only(left: 22),
                    itemCount: bloc.pokemonListStats.length,
                    itemBuilder: (context, index) {
                      PokemonStat pokemonStat = bloc.pokemonListStats[index];
                      return AttributeWidget(
                        nameAttr: pokemonStat.name,
                        stats: pokemonStat.baseStat,
                      );
                    },
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }

  Widget _statsList(BuildContext context) {
    return Consumer<PokemonDetailBLoC>(
      builder: (context, bloc, child) {
        if (bloc.pokemonListStats.length == 0) {
          return Container(
            width: MediaQuery.of(context).size.width - 20,
            height: MediaQuery.of(context).size.height / 2,
            padding: EdgeInsets.only(top: 50),
            child: Center(
              child: Text('Cargando...'),
            ),
          );
        } else {
          return Container(
            width: MediaQuery.of(context).size.width - 75,
            height: 650,
            margin: EdgeInsets.only(bottom: 25),
            decoration: BoxDecoration(
              boxShadow: BoxShadows.boxShadow,
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradientsColors.backgroundColor,
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 25,
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    'Description Stats',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                    ),
                  ),
                ),
                Expanded(
                  flex: 12,
                  child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: bloc.pokemonListStats.length,
                    itemBuilder: (context, index) {
                      PokemonStat pokemonStat = bloc.pokemonListStats[index];
                      return PokemonItemDescStat(
                        index: index,
                        nameAttr: pokemonStat.name,
                        stats: pokemonStat.baseStat,
                      );
                    },
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
