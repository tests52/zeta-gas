import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pokedex/data/api/pokemon_repository_impl.dart';
import 'package:pokedex/presentation/pages/splash/splash_screen.dart';
import 'package:pokedex/domain/repository/api/pokemon_repository.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // Unicamente se utiliza un solo provider para la implementacion, aun asi se utiliza clean arquitecture para todo el proceso.
        Provider<ApiPokemonRepositoryInterface>(
          create: (_) => ApiPokemonRepositoryImpl(),
        ),
      ],
      child: Builder(
        builder: (newContext) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            home: SplashScreen.init(newContext),
          );
        },
      ),
    );
  }
}
