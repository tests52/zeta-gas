# Pokedex

New Flutter project with pokeapi.co and clean code architecture. In which we can find a brief and concise reference to the use of clean architectures using Providers as a state manager in the following Pokedex application.

Some of the libraries used in this project are:

- http: ^ 0.12.0 + 4
- provider: 4.3.1
- google_fonts: any
- animate_do: ^ 1.7.3
- flutter_svg: ^ 0.19.0
- font_awesome_flutter: ^ 9.1.0

## Final project demonstration

![](/assets/markdown/demo.gif) 


## Getting Started

The present examination was carried out for the ZetaGas plant company, however for the integrity of the company it decided not to carry out the same design as the original, however I believe that this examination contains even better challenges to verify the feasibility of the technology and handling. method on Flutter.

![image](/assets/markdown/pokeapi_256.png) 


Currently the application starts with a Welcome screen, or known as SplashScreen, which at the end of the rendering of the screen sends to the Home screen or HomeScreen


![image](/assets/markdown/splash.png) 

In the home screen or HomeScreen you get the list of pokemons from the pokeapi.co api in which when you get the names of each one and the respective image for pokemon which is obtained from the id that comes from the URL, it is get the detail screen or DetailScreen

![image](/assets/markdown/home.png) 

When selecting a pokemon, the rendered item performs a pulse effect to give the impression that it is selecting a pokemon, the action when performing is to send the user to the DetailScreen screen of the pokemon obtained, the information is loaded after the loading text disappears , to obtain the information obtained through another URL of pokeapi.co provides we obtain the detail of Skills and Statistics, All content is displayed friendly and through an animated dashboard

![image](/assets/markdown/detail1.png) 

![image](/assets/markdown/detail2.png) 

## References

- Pokeapi:  https://pokeapi.co/
- Clean Code Architecture: https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html
- Reference design, to make my own design: https://dribbble.com/shots/6540871-Pokedex-App/attachments/6540871-Pokedex-App?mode=media
